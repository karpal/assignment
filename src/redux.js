import { createStore } from 'redux';

const initialState = {
    stocks: []
}

const StockActionTypes = {
    ADD_STOCK: 'Add Stock',
    UPDATE_STOCK: 'UPDATE Stock',
    DELETE_STOCK: 'Delete Stock'
}

export const store = createStore(
    reducer,
    initialState,
    window.devToolsExtension && window.devToolsExtension()
)

function reducer(state, { type, payload }) {
    switch (type) {
        case StockActionTypes.ADD_STOCK:
            return {
                ...state,
                stocks: [...state.stocks, payload]
            }
        case StockActionTypes.UPDATE_STOCK:
            return {
                ...state,
                stocks: state.stocks.map(stock => (stock.id === payload.id) ? { ...stock, date: payload.date, price: payload.price, quantity: payload.quantity } : stock)
            }
        case StockActionTypes.DELETE_STOCK:
            return {
                ...state,
                stocks: state.stocks.filter(stock => stock.id !== payload)
            }
        default:
            return state;
    }
}

export const addStockAction = (stock) => ({
    type: StockActionTypes.ADD_STOCK,
    payload: stock
})

export const updateStockAction = (stock) => ({
    type: StockActionTypes.UPDATE_STOCK,
    payload: stock
})

export const deleteStockAction = (id) => ({
    type: StockActionTypes.DELETE_STOCK,
    payload: id
})