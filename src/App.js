import React from 'react';
import './App.scss';
import StockList from './component/StockComponent/StockList';
import StockVisualisation from './component/StockComponent/StockVisualisation';

import { Provider } from 'react-redux';
import { store } from './redux';

const App = () => (
  <Provider store={store}>
    <div className='container'>
      <StockList />
      <StockVisualisation></StockVisualisation>
    </div>
  </Provider>
)

export default App;
