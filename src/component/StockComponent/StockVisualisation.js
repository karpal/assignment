import React, { useEffect, useRef, useState } from "react";
import { shallowEqual, useSelector } from 'react-redux';
import lodash from 'lodash';
import moment from 'moment';
import Chart from "chart.js";
import { Table } from 'react-bootstrap';

export default () => {
    const chartContainer = useRef(null);

    const stocks = useSelector((state) => state.stocks, shallowEqual)

    const [totalQuantity, setTotalQuantity] = useState(null)
    const [totalPrice, setTotalPrice] = useState(null)



    useEffect(() => {

        setTotalQuantity(stocks.reduce((quantity, stock) => { return quantity + Number(stock.quantity) }, 0))

        setTotalPrice(stocks.reduce((price, stock) => { return price + (Number(stock.quantity) * Number(stock.price)) }, 0))

        const sortedStocks = lodash.orderBy(stocks, ['date'], ['asc'])

        const prices = sortedStocks.map((stock, index) => {
            return stock.price
        })

        const quantities = sortedStocks.map((stock, index) => {
            return stock.quantity
        })

        const calculateDays = (basedate, dateTocalculateFrom) => {
            return moment(dateTocalculateFrom, "DD/MM/YYYY").diff(moment(basedate, "DD/MM/YYYY"), 'days')
        }

        const days = sortedStocks.map((stock, index) => {
            return calculateDays(sortedStocks[0].date, stock.date)
        })

        if (chartContainer && chartContainer.current) {

            const chartConfig = {
                type: "line",
                data: {
                    labels: days,
                    datasets: [
                        {
                            label: "Price",
                            data: prices,
                            backgroundColor: [
                                "rgba(255, 99, 132, 0.2)"
                            ],
                            borderColor: [
                                "rgba(255, 99, 132, 1)"
                            ],
                            borderWidth: 1
                        },
                        {
                            label: "Quantity",
                            data: quantities,
                            backgroundColor: [
                                "rgba(54, 162, 235, 0.2)"
                            ],
                            borderColor: [
                                "rgba(54, 162, 235, 1)"
                            ],
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    beginAtZero: true
                                }
                            }
                        ]
                    }
                }
            }

            new Chart(chartContainer.current, chartConfig)
        }
    }, [chartContainer, stocks])



    return (
        <div className="container">
            <h3>Stocks</h3>
            <canvas ref={chartContainer} />
            <Table striped bordered hover >
                <tbody>
                    <tr>
                        <td>Total Quantity</td>
                        <td>{totalQuantity}</td>
                    </tr>
                    <tr>
                        <td>Total Price</td>
                        <td>$ {Number(totalPrice).toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td>Items in list</td>
                        <td>{stocks.length}</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    );
};



