import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { updateStockAction, addStockAction } from '../../redux';
import lodash from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';


const StockFormModal = ({ show, stockId, onHideFn }) => {

    const dispatch = useDispatch()

    const stocks = useSelector((state) => state.stocks, shallowEqual)

    const [selectedDate, setSelectedDate] = useState(null)
    const [price, setPrice] = useState('')
    const [quantity, setQuantity] = useState('')
    const [submitButtonStateDisabled, setSubmitButtonStateDisabled] = useState(true)

    const excludedDatesFn = () => {
        return stockId ? stocks.filter(currentStock => currentStock.id !== stockId).map(stock => { return moment(stock.date, 'DD/MM/YYYY').toDate() }) : stocks.map(stock => { return moment(stock.date, 'DD/MM/YYYY').toDate() })
    }
    const excludeDate = excludedDatesFn()

    const priceChange = (event) => {
        if (lodash.isEmpty(event.target.value)) {
            setPrice('')
        }

        if (Number(event.target.value) !== 0) {
            setPrice(event.target.value)
        }

        validateFn()
    }

    const quantityChange = (event) => {
        if (lodash.isEmpty(event.target.value)) {
            setQuantity('')
        }

        if (Number(event.target.value) !== 0) {
            setQuantity(event.target.value)
        }

        validateFn()
    }

    const validateFn = () => {
        if (!lodash.isNull(selectedDate) && lodash.isDate(selectedDate)) {
            if (Number(price) !== 0) {
                if (Number(quantity) !== 0) {
                    setSubmitButtonStateDisabled(false)
                } else {
                    setSubmitButtonStateDisabled(true)
                }
            } else {
                setSubmitButtonStateDisabled(true)
            }
        } else {
            setSubmitButtonStateDisabled(true)
        }
    }

    const clearFormFn = () => {
        setSelectedDate(null)
        setPrice('')
        setQuantity('')
        setSubmitButtonStateDisabled(true)
    }

    const LoadFormFn = (stockId) => {
        useEffect(() => {
            if (!lodash.isNil(stockId) && !lodash.isEmpty(stockId)) {
                const foundStocks = stocks.filter(stock => stock.id === stockId)
                const stock = foundStocks[0] 
                setSelectedDate(moment(stock.date, 'DD/MM/YYYY').toDate())
                setPrice(stock.price)
                setQuantity(stock.quantity)
                setSubmitButtonStateDisabled(false)
            }
        }, [stockId])
    }

    const onBlurDatePickerFn = (event) => {
        if (lodash.isEmpty(event.target.value)) {
            setSelectedDate(null)
        }

        validateFn()
    }

    const onSubmitFn = (event) => {
        event.preventDefault()

        if (lodash.isNil(stockId) && lodash.isEmpty(stockId)) {
            const dateString = moment(selectedDate).format('DD/MM/yyyy')
            
            const stock = {
                id: uuidv4(),
                date: dateString,
                price: price,
                quantity: quantity
            }

            dispatch(addStockAction(stock))

        } else {
            const dateString = moment(selectedDate).format('DD/MM/yyyy')
            
            const stock = {
                id: stockId,
                date: dateString,
                price: price,
                quantity: quantity
            }

            dispatch(updateStockAction(stock))

        }

        clearFormFn()
        onHideFn()
    }

    LoadFormFn(stockId)

    return (
        <Modal
            animation={false}
            show={show}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            onHide={onHideFn}
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {
                        stockId ? 'Update Stock' : 'Add Stock'
                    }
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className='container'>
                    <form onSubmit={onSubmitFn}>
                        Date: <DatePicker
                            selected={selectedDate}
                            onChange={date => {
                                setSelectedDate(date)
                                validateFn()
                            }}
                            dateFormat='dd/MM/yyyy'
                            placeholderText='Please select a date'
                            excludeDates={excludeDate}
                            onBlur={onBlurDatePickerFn}
                        />
                        <br />
                        <br />
                        Price: $
                        <input
                            id='priceInput'
                            type='number'
                            placeholder='Enter price'
                            step='0.1'
                            onChange={priceChange}
                            value={price}
                        />
                        <br />
                        <br />
                        Quantity:
                        <input
                            type='number'
                            placeholder='Enter quantity'
                            onChange={quantityChange}
                            value={quantity}
                        />
                        <div className='buttonContainer'>
                            <Button variant='danger' onClick={() => {
                                clearFormFn()
                                onHideFn()
                            }} className='buttonModal'>Close</Button>
                            {
                                stockId ? <Button type='submit' variant='primary' disabled={submitButtonStateDisabled} className='buttonModal'>Update</Button>
                                    : <Button type='submit' variant='primary' disabled={submitButtonStateDisabled} className='buttonModal'>Add</Button>
                            }
                        </div>
                    </form>
                </div>
            </Modal.Body>
            <Modal.Footer>

            </Modal.Footer>
        </Modal>
    )
}

export default StockFormModal