import React, { useState } from 'react';
import { shallowEqual, useSelector, useDispatch } from 'react-redux';
import { deleteStockAction } from '../../redux';
import { Table } from 'react-bootstrap';
import { BsPencil, BsTrash } from 'react-icons/bs';
import { FaPlus } from 'react-icons/fa';
import StockFormModal from './StockFormModal';


export default () => {

    const dispatch = useDispatch()

    const stocks  = useSelector((state) => state.stocks, shallowEqual)

    const [showModal, setShowModal] = useState(false)
    const [stockId, setStockId] = useState('')

    const hideStockFormModal = () => {
        setShowModal(false)
    }

    const showStockFormModal = (id) => {
        setShowModal(true)
        setStockId(id)
    }


    const renderStock = (stock) => {
        return (
            <tr key={stock.id}>
                <td>{stock.date}</td>
                <td>$ {Number(stock.price).toFixed(2)}</td>
                <td>{stock.quantity}</td>
                <td><span className='actions' onClick={() => showStockFormModal(stock.id)}><BsPencil /></span> <span className='actions' onClick={() => dispatch(deleteStockAction(stock.id))}><BsTrash /></span></td>
            </tr>
        )
    }

    return (
        <div className='container'>
            <Table striped bordered hover variant='dark'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        Array.isArray(stocks) && stocks.length ? stocks.map(renderStock) : null
                    }
                    <tr>
                        <td><span className='actions' onClick={() => showStockFormModal()}><FaPlus /></span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
                <StockFormModal show={showModal} onHideFn={hideStockFormModal} stockId={stockId} />
            </Table>
        </div>
    )
}