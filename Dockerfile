# pull official base image
FROM node:14.4-alpine3.11

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH


# add app
COPY . ./

# install dependencies
RUN yarn install

# start app
CMD ["yarn", "start"]