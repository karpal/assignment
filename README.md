This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Build and start the container: docker-compose up -d --build

Once container is successfully built, can be accessed from the browser using: http://localhost

Stop and remove the container: docker-compose down
